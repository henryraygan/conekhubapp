-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 06-06-2018 a las 20:41:47
-- Versión del servidor: 10.2.14-MariaDB
-- Versión de PHP: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `conekhub_migration_test`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `careers`
--

CREATE TABLE `careers` (
  `id_career` int(11) NOT NULL,
  `nameCareer` varchar(200) NOT NULL,
  `school` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `careers`
--

INSERT INTO `careers` (`id_career`, `nameCareer`, `school`) VALUES
(1, 'Contaduría Pública', 1),
(2, 'Licenciatura en Administración', 1),
(3, 'Licenciatura en Gastronomía', 2),
(4, 'Licenciatura en Negocios Internacionales', 2),
(5, 'Ingeniería en Mantenimiento Industrial', 3),
(6, 'Ingeniería Financiera y Fiscal', 3),
(7, 'Ingeniería en Sistemas Computacionales', 1),
(8, 'Licenciatura en Innovación Empresarial', 2),
(9, 'Licenciatura en Turismo Sustentable y Gestión Hotelera', 2),
(10, 'Ingeniería Industrial', 2),
(11, 'Ingeniería de Datos e Inteligencia Organizacional', 2),
(12, 'Ingeniería en Logística y Cadena de Suministro', 2),
(13, 'Ingeniería Civil', 1),
(14, 'Ingeniería Electromecánica', 1),
(15, 'Ingeniería en Administracion', 1),
(16, 'Ingeniería en Gestion Empresarial', 1),
(17, 'Ingeniería en Mecatronica', 1),
(18, 'Ingeniería en Informatica', 1),
(19, 'Ingeniería Ambiental', 2),
(20, 'Turismo Alternativo y Gestión del Patrimonio', 2),
(21, 'Ingeniería en Desarrollo e Innovación Empresarial', 3),
(22, 'Licenciatura en Gastronomía', 3),
(23, 'Ingeniería en Tecnologías de la Información y Comunicación', 3),
(24, 'Licenciatura en Gestión y Desarrollo Turístico', 3),
(25, 'Ingeniería en Biotecnología', 4),
(26, 'Ingeniería en Software', 4),
(27, 'Ingeniería Financiera', 4),
(28, 'Ingeniería en Biomédica', 4),
(29, 'Licenciatura en Administración y Gestión de PyMEs', 4),
(30, 'Licenciatura en Terapia Física', 4),
(31, 'Administración Hotelera', 5),
(32, 'Ingeniería en Redes', 5),
(33, 'Derecho', 5),
(34, 'Licenciatura en Mercadotecnia y Negocios', 5),
(35, 'Direccion y Administración de Empresas', 6),
(36, 'Negocios Internacionales', 6),
(37, 'Finanzas y Contaduria Publica', 6),
(38, 'Mercadoctenia', 6),
(39, 'Ingeniería Civil', 6),
(40, 'ingeniería Industrial', 6),
(41, 'Medicina', 6),
(42, 'Nutrición', 6),
(43, 'Direccion Internacional de Hoteles', 6),
(44, 'Turismo Internacional', 6),
(45, 'Gastronomía', 6),
(46, 'Comunicación', 6),
(47, 'Dirección de Empresas de Entretenimiento', 6),
(48, 'Diseño', 6),
(49, 'Arquitectura', 6),
(50, 'Derecho', 6),
(51, 'Relaciones Internacionales', 6),
(52, 'Psicología', 6),
(53, 'Comunicación', 7),
(54, 'Psicología', 7),
(55, 'Derecho', 7),
(56, 'Mercadoctenia', 7),
(57, 'Diseño Publicitario', 7),
(58, 'Arquitectura', 7),
(59, 'Gastroomía', 7),
(60, 'Sistemas Computacionales', 7),
(61, 'Comercio Internacional', 7),
(62, 'Contaduría Publica', 7),
(63, 'Administración', 7),
(64, 'Ciencias Politicas', 7),
(65, 'Fisioterapia', 7),
(66, 'Lenguas Extranjeras', 7),
(67, 'Administración', 8),
(68, 'Arquitectura', 8),
(69, 'Ciencias de la Comunicación', 8),
(70, 'Contaduría y Finanzas', 8),
(71, 'Criminología y Criminalística', 8),
(72, 'Derecho', 8),
(73, 'Diseño Gráfico y Digital', 8),
(74, 'Enfermería', 8),
(75, ' Ingeniería Civil', 8),
(76, 'Mercadotecnia', 8),
(77, 'Nutrición y Gastronomía', 8),
(78, 'Psicología', 8),
(79, 'Administración de Empresas Turísticas', 8),
(80, 'Idiomas y Relaciones Públicas', 8),
(81, 'Nutrición', 9),
(82, 'Psicología', 9),
(83, 'Derecho', 9),
(84, 'Ingeniería Industrial', 9),
(85, 'Ingeniería en Mecatrónica', 9),
(86, 'Administración de Empresas', 9),
(87, 'Administración Financiera', 9),
(88, 'Administración Hotelera y Turistica', 9),
(89, 'Comercio Internacional', 9),
(90, 'Gastronomía', 9),
(91, 'Mercadoctecnia', 9),
(92, 'Diseño Grafico y Animación', 9),
(93, 'Desarrollo de Software', 9),
(94, 'Sistemas de Computación Administrativa', 9),
(95, 'Pedagogía', 10),
(96, 'Ciencias de la Comunicación', 10),
(97, 'Odontología', 10),
(98, 'Administración de la Hospitalidad', 10),
(99, 'Derecho', 10),
(100, 'Contaduría Pulica', 10),
(101, 'Comercio exterior y Aduanas', 10),
(102, 'Administración de Empresas', 10),
(103, 'Relaciones Internacionales', 10),
(104, 'Psicología Organizacional', 10),
(105, 'Trabajo Social', 10),
(106, 'Sistemas Computacionales', 10),
(107, 'Diseño Grafico', 10),
(108, 'Arquitectura', 10);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estudent`
--

CREATE TABLE `estudent` (
  `id` int(11) NOT NULL,
  `id_school` int(11) NOT NULL,
  `id_career` int(11) NOT NULL,
  `url_profile` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `experiences`
--

CREATE TABLE `experiences` (
  `id_experience` int(11) NOT NULL,
  `employment` varchar(200) NOT NULL,
  `ingress` date NOT NULL,
  `egress` date DEFAULT NULL,
  `actual` tinyint(1) NOT NULL DEFAULT 0,
  `id_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `following`
--

CREATE TABLE `following` (
  `user_id` int(11) NOT NULL,
  `follower_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `post`
--

CREATE TABLE `post` (
  `PostId` int(11) NOT NULL,
  `PostTitle` varchar(200) NOT NULL,
  `PostContent` varchar(500) NOT NULL,
  `PostDate` timestamp NOT NULL DEFAULT current_timestamp(),
  `OwnerID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `recluter`
--

CREATE TABLE `recluter` (
  `id` int(11) NOT NULL,
  `empresa` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `schools`
--

CREATE TABLE `schools` (
  `id_school` int(11) NOT NULL,
  `nameSchool` varchar(200) NOT NULL,
  `shortName` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `schools`
--

INSERT INTO `schools` (`id_school`, `nameSchool`, `shortName`) VALUES
(1, 'Instituto Tecnológico de Cancún ', 'ITC'),
(2, 'Universidad del Caribe ', 'Unicaribe'),
(3, 'Universidad Tecnológica de Cancún ', 'UT'),
(4, 'Universidad Politecnica', 'UPQROO'),
(5, 'Universidad de Quintanaroo', 'UQROO'),
(6, 'Universidad Anáhuac del Sur', 'Anáhuac'),
(7, 'Universidad de Oriente', 'UO'),
(8, 'Universidad La Salle de México', 'La Salle'),
(9, 'Universidad TecMilenio', 'TecMilenio'),
(10, 'Universidad del Sur', 'US'),
(11, 'Universidad Maya', 'UM'),
(12, 'Universidad Humanitas', 'Humanitas');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `school_grades`
--

CREATE TABLE `school_grades` (
  `id_grade` int(11) NOT NULL,
  `grade` varchar(12) NOT NULL,
  `num` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `firstName` varchar(70) NOT NULL,
  `lastName` varchar(70) NOT NULL,
  `email` varchar(255) NOT NULL,
  `pass` varchar(32) NOT NULL,
  `rol` varchar(10) NOT NULL,
  `isActive` varchar(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `careers`
--
ALTER TABLE `careers`
  ADD PRIMARY KEY (`id_career`),
  ADD KEY `school` (`school`);

--
-- Indices de la tabla `estudent`
--
ALTER TABLE `estudent`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_school` (`id_school`),
  ADD KEY `id_career` (`id_career`);

--
-- Indices de la tabla `experiences`
--
ALTER TABLE `experiences`
  ADD PRIMARY KEY (`id_experience`),
  ADD KEY `id_user` (`id_user`);

--
-- Indices de la tabla `following`
--
ALTER TABLE `following`
  ADD PRIMARY KEY (`user_id`,`follower_id`),
  ADD UNIQUE KEY `follower_id` (`follower_id`,`user_id`);

--
-- Indices de la tabla `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`PostId`),
  ADD KEY `OwnerID` (`OwnerID`);

--
-- Indices de la tabla `recluter`
--
ALTER TABLE `recluter`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `schools`
--
ALTER TABLE `schools`
  ADD PRIMARY KEY (`id_school`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `careers`
--
ALTER TABLE `careers`
  MODIFY `id_career` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=109;

--
-- AUTO_INCREMENT de la tabla `experiences`
--
ALTER TABLE `experiences`
  MODIFY `id_experience` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `post`
--
ALTER TABLE `post`
  MODIFY `PostId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `schools`
--
ALTER TABLE `schools`
  MODIFY `id_school` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `careers`
--
ALTER TABLE `careers`
  ADD CONSTRAINT `careers_ibfk_1` FOREIGN KEY (`school`) REFERENCES `schools` (`id_school`);

--
-- Filtros para la tabla `estudent`
--
ALTER TABLE `estudent`
  ADD CONSTRAINT `estudent_ibfk_1` FOREIGN KEY (`id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `estudent_ibfk_2` FOREIGN KEY (`id_school`) REFERENCES `schools` (`id_school`),
  ADD CONSTRAINT `estudent_ibfk_3` FOREIGN KEY (`id_career`) REFERENCES `careers` (`id_career`);

--
-- Filtros para la tabla `experiences`
--
ALTER TABLE `experiences`
  ADD CONSTRAINT `experiences_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `following`
--
ALTER TABLE `following`
  ADD CONSTRAINT `following_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `following_ibfk_2` FOREIGN KEY (`follower_id`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `post`
--
ALTER TABLE `post`
  ADD CONSTRAINT `post_ibfk_1` FOREIGN KEY (`OwnerID`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `recluter`
--
ALTER TABLE `recluter`
  ADD CONSTRAINT `recluter_ibfk_1` FOREIGN KEY (`id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
