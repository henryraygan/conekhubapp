export const environment = {
  production: false,
  apiUrl: 'http://api.conekhub.com/api/v2',
  ttltoken: 10,
  endpoints: {
    users: '/users',
    user: '/users/:id',
    usersLogin: '/users/login',
    userNew: '/users/new',
    userAddSchool: '/users/add/school',
    userAddPost: '/users/add/post',
    userActivated: '/users/activated',
    userFollowing: '/users/following/:id',
    userFollowers: '/users/followers/:id',
    userFollowed: '/users/followed', 
    userUnfollow: '/users/unfollow',
    userUrlExists: '/users/url/exists',
    userEmailExists: '/users/email/exists',
    schools: '/schools',
    schoolsCareers: '/schools/:id/careers'
  }
};
