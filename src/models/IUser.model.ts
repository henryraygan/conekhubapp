export interface UserLogin {
    email: string,
    password: string
}

export interface UserRegister {
    name: string,
    lastname: string,
    email: string,
    password: string,
    role: string,
    status: false
}

export interface UserFollowed {
    id_user: string,
    id_follow: string
}

export interface UserAddSchool {
    id_user: string,
    id_school: string,
    id_career: string,
    url: string
}

export interface UserAddPost {
    title: string,
    content: string,
    owner_id: string
}

export interface UserCheckingUrl {
    url: string
}

export interface UserCheckingEmail {
    email: string
}

