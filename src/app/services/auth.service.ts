import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UserLogin } from '../../models/IUser.model';
import { AppSettings } from '../settings/app.setting';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private _config: AppSettings
  ) { }


  login(user: UserLogin): Observable<any> {
    let URL = this._config.ENDPOINT_USER_LOGIN;

    const data = Observable.create(observer => {
      const body = JSON.stringify(user);

      fetch(this._config.ENDPOINT_USER_LOGIN, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json'
        },
        body: body
      })
        .then(response => response.json()) // or text() or blob() etc.
        .then(data => {
          observer.next(data);
          observer.complete();
        })
        .catch(err => observer.error(err));
    });

    return data;

  }

}
