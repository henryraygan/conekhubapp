import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AppSettings } from '../settings/app.setting';
import { ConekhubService } from '../services/conekhub.service';
import { UserRegister } from '../../models/IUser.model';


@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  constructor(
    private _config: AppSettings
  ) { }

  RegisterUser(user: UserRegister): Observable<any> {

    const data = Observable.create(observer => {
      const body = JSON.stringify(user);

      fetch(this._config.ENDPOINT_REGISTER_USER, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json'
        },
        body: body
      })
        .then(response => response.json()) // or text() or blob() etc.
        .then(data => {
          observer.next(data);
          observer.complete();
        })
        .catch(err => observer.error(err));
    });

    return data;

  }

}
