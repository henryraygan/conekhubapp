import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ConekhubService {

  constructor() { }

  public injectParamsInUrl(urlString: string, paramObject: any): string {
    if (typeof paramObject === 'undefined') {
      return urlString;
    }
    for (const p in paramObject) {
      if (paramObject.hasOwnProperty(p)) {
        const stringified = paramObject[p];
        urlString = urlString.replace(':' + p, stringified);
      }
    }
    return urlString;
  }

}
