import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AppSettings } from '../settings/app.setting';
import { ConekhubService } from '../services/conekhub.service';


@Injectable({
  providedIn: 'root'
})
export class SchoolsService {

  constructor(
    private _config: AppSettings,
    private _conekhub: ConekhubService
  ) { }


  getSchools(): Observable<any> {
    const data = Observable.create(observer => {
      fetch(this._config.ENDPOINT_GET_SCHOOLS) // Call the fetch function passing the url of the API as a parameter
        .then(response => response.json()) // or text() or blob() etc.
        .then(data => {
          observer.next(data);
          observer.complete();
        })
        .catch(err => observer.error(err));
    });
    return data;
  }

  getCareers(id: number): Observable<any> {
    let URL = this._config.ENDPOINT_GET_CAREERS;
    URL = this._conekhub.injectParamsInUrl(URL, { id: id });
    const data = Observable.create(observer => {
      fetch(URL)
        .then(response => response.json()) 
        .then(data => {
          observer.next(data);
          observer.complete();
        })
        .catch(err => observer.error(err));
    });
    return data;
  }


}
