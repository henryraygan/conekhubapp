import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AuthService } from '../app/services/auth.service';
import { ConekhubService } from '../app/services/conekhub.service';
import { SchoolsService } from '../app/services/schools.service';
import { RegisterService } from '../app/services/register.service';
import { AppSettings } from '../app/settings/app.setting';



import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { InputPasswordComponent } from './components/shared/input-password/input-password.component';
import { LandingComponent } from './components/landing/landing.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { RegisterComponent } from './components/register/register.component';
import { FeedComponent } from './components/feed/feed.component';
import { ProfilePublicComponent } from './components/profile-public/profile-public.component';
import { ConexionesComponent } from './components/conexiones/conexiones.component';
import { MiPerfilComponent } from './components/mi-perfil/mi-perfil.component';
import { OfertasComponent } from './components/ofertas/ofertas.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    InputPasswordComponent,
    LandingComponent,
    PageNotFoundComponent,
    RegisterComponent,
    FeedComponent,
    ProfilePublicComponent,
    ConexionesComponent,
    MiPerfilComponent,
    OfertasComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
  ],
  providers: [
    AuthService,
    ConekhubService,
    SchoolsService,
    RegisterService,
    AppSettings
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
