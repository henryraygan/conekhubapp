import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';

@Injectable()
export class AppSettings {

  readonly API_URL: string;
  readonly ENDPOINT_USER_LOGIN: string;
  readonly ENDPOINT_GET_USERS: string;
  readonly ENDPOINT_GET_USER: string;
  readonly ENDPOINT_REGISTER_USER: string;
  readonly ENDPOINT_GET_SCHOOLS: string;
  readonly ENDPOINT_GET_CAREERS: string;

 

  constructor() {
    const env = environment;
    this.API_URL = env.apiUrl;
    this.ENDPOINT_USER_LOGIN = env.apiUrl + env.endpoints.usersLogin;
    this.ENDPOINT_GET_USERS = env.apiUrl + env.endpoints.users;
    this.ENDPOINT_GET_USER = env.apiUrl + env.endpoints.user;
    this.ENDPOINT_REGISTER_USER = env.apiUrl + env.endpoints.userNew;
    this.ENDPOINT_REGISTER_USER = env.apiUrl + env.endpoints.userNew;
    this.ENDPOINT_GET_SCHOOLS = env.apiUrl + env.endpoints.schools;
    this.ENDPOINT_GET_CAREERS = env.apiUrl + env.endpoints.schoolsCareers;
  }

}
