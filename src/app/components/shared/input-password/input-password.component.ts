import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'input-password',
  templateUrl: './input-password.component.html',
  styleUrls: ['./input-password.component.scss']
})
export class InputPasswordComponent implements OnInit {

  type = 'password';
  icon = 'lock';
  placeholder = '* * * * * *';
  clicked: boolean;

  constructor() { }

  ngOnInit() {
  }

  viewPassword(e) {
    if(this.type === 'password') this.type = 'text';
    else this.type = 'password';

    if(!this.clicked) this.clicked = true;
    else this.clicked = false;

  }

}
