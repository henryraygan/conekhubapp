import { Component, OnInit } from '@angular/core';
import { UserRegister } from "../../../models/IUser.model";
import { RegisterService } from '../../services/register.service';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  newUser: UserRegister = {
    name: "",
    lastname: "",
    email: "",
    password: "",
    role: "",
    status: false
  }

  constructor(
    private _register: RegisterService
  ) { }

  ngOnInit() {
  }


  registrar(){
    console.log(this.newUser);
  }

}
