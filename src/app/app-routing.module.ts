import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { LoginComponent } from '../app/components/login/login.component';
import { LandingComponent } from '../app/components/landing/landing.component';
import { PageNotFoundComponent } from '../app/components/page-not-found/page-not-found.component';
import { RegisterComponent } from '../app/components/register/register.component';
import { FeedComponent } from '../app/components/feed/feed.component';
import { ProfilePublicComponent } from '../app/components/profile-public/profile-public.component';
import { OfertasComponent } from '../app/components/ofertas/ofertas.component'

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'landing'
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'register',
    component: RegisterComponent
  },
  {
    path: 'feed',
    component: FeedComponent
  },
  {
    path: 'public/in/:url',
    component: ProfilePublicComponent
  },
  {
    path: 'buscar',
    component: OfertasComponent
  },
  {
    path: 'landing',
    component: LandingComponent
  },
  {
    path: '404',
    component: PageNotFoundComponent
  },
  {
    path: '**',
    pathMatch: 'full',
    redirectTo: '404'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
